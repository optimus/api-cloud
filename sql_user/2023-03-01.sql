CREATE TABLE IF NOT EXISTS `calendars` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`server` varchar(128) DEFAULT NULL,
	`user` int(10) unsigned DEFAULT NULL,
	`calendar` int(10) unsigned DEFAULT NULL,
	`name` varchar(64) DEFAULT NULL,
	`color` varchar(6) DEFAULT 'ff6384',
	`display` bit(1) NOT NULL DEFAULT b'1',
	`displayname` varchar(128) GENERATED ALWAYS AS (`name`) VIRTUAL,
	PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;

INSERT INTO `calendars` SET `name` = 'Agenda principal';

CREATE TABLE IF NOT EXISTS `calendars_events` (
	`id` int unsigned NOT NULL AUTO_INCREMENT,
	`calendar` int unsigned DEFAULT NULL,
	`title` varchar(64) DEFAULT NULL,
	`notes` text DEFAULT NULL,
	`start` datetime DEFAULT NULL,
	`end` datetime DEFAULT NULL,
	`allday` bit NOT NULL DEFAULT b'0',
	`rrule` text DEFAULT NULL,
	`vevent` text DEFAULT NULL,
	PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `calendars_events_exdates` (
	`id` int unsigned NOT NULL AUTO_INCREMENT,
	`event` int unsigned DEFAULT NULL,
	`date` datetime DEFAULT NULL,
	PRIMARY KEY (`id`) USING BTREE,
	KEY `events_to_exdates` (`event`),
	CONSTRAINT `events_to_exdates` FOREIGN KEY (`event`) REFERENCES `calendars_events` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `calendars_events_properties` (
	`id` int unsigned NOT NULL AUTO_INCREMENT,
	`event` int unsigned DEFAULT NULL,
	`property` varchar(32) DEFAULT NULL,
	`value` varchar(255) DEFAULT NULL,
	PRIMARY KEY (`id`) USING BTREE,
	KEY `events_to_properties` (`event`),
	CONSTRAINT `events_to_properties` FOREIGN KEY (`event`) REFERENCES `calendars_events` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `contacts` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`categorie` tinyint(3) unsigned DEFAULT 0,
	`type` tinyint(3) unsigned NOT NULL DEFAULT 10,
	`title` tinyint(3) unsigned NOT NULL DEFAULT 0,
	`firstname` varchar(255) DEFAULT NULL,
	`lastname` varchar(255) DEFAULT NULL,
	`birth_date` date DEFAULT NULL,
	`birth_zipcode` varchar(16) DEFAULT NULL,
	`birth_city` varchar(255) DEFAULT NULL,
	`birth_city_name` varchar(255) DEFAULT NULL,
	`birth_country` smallint(5) unsigned NOT NULL DEFAULT 74,
	`insee` varchar(255) DEFAULT NULL,
	`marital_status` tinyint(3) unsigned NOT NULL DEFAULT 0,
	`maiden_name` varchar(255) DEFAULT NULL,
	`company_name` varchar(255) DEFAULT NULL,
	`company_type` smallint(5) unsigned NOT NULL DEFAULT 0,
	`company_capital` varchar(255) DEFAULT NULL,
	`siret` varchar(255) DEFAULT NULL,
	`company_greffe` varchar(4) DEFAULT NULL,
	`address` text DEFAULT NULL,
	`zipcode` varchar(255) DEFAULT NULL,
	`city` varchar(255) DEFAULT NULL,
	`city_name` varchar(255) DEFAULT NULL,
	`country` int(10) unsigned NOT NULL DEFAULT 74,
	`phone` varchar(255) DEFAULT NULL,
	`fax` varchar(255) DEFAULT NULL,
	`mobile` varchar(255) DEFAULT NULL,
	`email` varchar(255) DEFAULT NULL,
	`website` varchar(255) DEFAULT NULL,
	`pro_phone` varchar(255) DEFAULT NULL,
	`pro_fax` varchar(255) DEFAULT NULL,
	`pro_mobile` varchar(255) DEFAULT NULL,
	`pro_email` varchar(255) DEFAULT NULL,
	`pro_website` varchar(255) DEFAULT NULL,
	`iban` varchar(255) DEFAULT NULL,
	`bic` varchar(255) DEFAULT NULL,
	`tva` varchar(255) DEFAULT NULL,
	`notes` text DEFAULT NULL,
	`displayname` varchar(128) GENERATED ALWAYS AS (coalesce(nullif(trim(concat(ucase(ifnull(`company_name`,'')),' ',ucase(ifnull(`firstname`,'')),' ',ifnull(`lastname`,''))),''),'????????')) VIRTUAL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;