<?php
$resource = json_decode('
{
	"id": { "type": "integer", "post": ["ignored"], "patch": ["immutable"], "default": 0},
	"server": { "type": "domain", "post": ["required"]},
	"user": { "type": "integer", "post": ["required"]},
	"calendar": { "type": "integer", "post": ["required"]},
	"name": { "type": "string", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": "Agenda N° "},
	"color": { "type": "hexcolor", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": "485fc7"},
	"display": { "type": "boolean", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": 1},
	"displayname" : { "type" : "string", "field": "calendars.displayname", "virtual": true }
}
', null, 512, JSON_THROW_ON_ERROR);

function get()
{
	global $optimus_connection, $resource, $input;
	auth();
	allowed_origins_only();
		
	$input->owner = check('owner', $input->path[1], 'integer', true);
	$input->db = get_user_db($input->owner);
	
	if ($input->user->id != $input->owner AND !is_admin($input->user->id))
	{
		$restrictions = get_restrictions_list($input->user->id, $input->owner, 'calendars');
		if (array_count_values(array_column($restrictions, 0))['read'] == sizeof($restrictions))
			return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour lister les agendas de cet utilisateur");
		foreach ($restrictions as $key => $restriction)
			if (in_array('read', $restriction))
				$exclusion_list[] = (int)explode('/', $key)[1];
	}

	$calendars = $optimus_connection->prepare("SELECT * FROM `" . $input->db . "`.`calendars`");
	
	if($calendars->execute())
		if ($calendars->rowCount() == 0)
			return array("code" => 204, "message" => "Cet utilisateur n'a ni agenda ni souscriptions");
		else
		{
			$calendars = $calendars->fetchAll(PDO::FETCH_ASSOC);
			
			for ($i=0; $i < sizeof($calendars); $i++)
			{
				if ($calendars[$i]['server'] == '')
				{
					unset($calendars[$i]['server']);
					unset($calendars[$i]['user']);
					unset($calendars[$i]['calendar']);
				}
				else
				{
					$calendars[$i]['owner']['id'] = (int)$calendars[$i]['user'];
					$calendars[$i]['owner']['server'] = $calendars[$i]['server'];
					$calendars[$i]['owner']['calendar'] = (int)$calendars[$i]['calendar'];
					unset($calendars[$i]['server']);
					unset($calendars[$i]['user']);
					unset($calendars[$i]['calendar']);
				}
				if (sizeof($restrictions['calendars/' . $calendars[$i]['id']]) > 0)
					$calendars[$i]['restrictions'] = $restrictions['calendars/' . $calendars[$i]['id']];
				else if (sizeof($restrictions['calendars']) > 0)
					$calendars[$i]['restrictions'] = $restrictions['calendars'];
			}
			return array("code" => 200, "data" => sanitize($resource, $calendars));
		}
	else
		return array("code" => 400, "message" => $calendars->errorInfo()[2]);
}
?>
