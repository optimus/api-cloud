<?php
include_once 'api_optimus-server/datatables.php';
$resource = json_decode('
{
	"id": { "type": "integer", "field": "calendars.id", "post": ["ignored"], "patch": ["immutable"], "default": 0},
	"name": { "type": "string", "field": "calendars.name", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": "NOUVEL AGENDA ' . time() . '"},
	"color": { "type": "hexcolor", "field": "calendars.color", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": "485fc7"},
	"display": { "type": "boolean", "field": "calendars.display", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": 1},
	"server": { "type": "domain", "field": "calendars.server", "post": ["ignored"], "patch": ["immutable"]},
	"displayname" : { "type" : "string", "field": "calendars.displayname", "virtual": true }
}
', null, 512, JSON_THROW_ON_ERROR);


function get()
{
	global $optimus_connection, $resource, $input;
	auth();
	allowed_origins_only();
		
	$input->id = check('id', $input->path[3], 'integer', false);
	$input->owner = check('owner', $input->path[1], 'integer', true);
	$input->db = get_user_db($input->owner);
	
	if ($input->user->id != $input->owner AND !is_admin($input->user->id))
		if (isset($input->id))
		{
			$restrictions = get_restrictions($input->user->id, $input->owner, 'calendars/' . $input->id);
			if (in_array('read', $restrictions))
				return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour lire cet agenda");
		}
		else
		{
			$restrictions = get_restrictions_list($input->user->id, $input->owner, 'calendars');
			if (sizeof($restrictions) > 0 AND array_count_values(array_column($restrictions, 0))['read'] == sizeof($restrictions))
				return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour lister les agendas de cet utilisateur");
		}

	//REQUETE SUR UN AGENDA IDENTIFIÉ
	if (isset($input->id))
	{
		$input->body = json_decode('{"filter": [{"field": "id", "type": "=", "value": ' . $input->id . '}, {"field": "server", "type": "isnull"}]}', null, 512, JSON_THROW_ON_ERROR);
		$results = datatable_request($optimus_connection, $resource, $input->db, 'calendars');
		if (sizeof($results) == 0)
			return array("code" => 404, "message" => "Cet agenda n'existe pas");
		else
			return array("code" => 200, "data" => sanitize($resource, array_merge($results[0], ['restrictions' => $restrictions])));
	}
	//REQUETE SUR TOUS LES AGENDAS AU FORMAT DATATABLES
	else 
	{	
		$input->body->filter[] = (object)array("field"=> "server", "type"=> "isnull");
		$results = datatable_request($optimus_connection, $resource, $input->db, 'calendars', $restrictions);
		$results = array_map(function ($result){unset($result['server']);return $result;}, $results);
		$last_row = (int)$optimus_connection->query('SELECT FOUND_ROWS()')->fetchColumn();
		$last_page = $input->body->size > 0 ? ceil(max($last_row,1) / $input->body->size) : 1;
		for ($i=0; $i < sizeof($results); $i++)
			$results[$i]['restrictions'] = $restrictions['calendars/' . $results[$i]['id']] ?? array_diff($restrictions['calendars'],['create']);
			
		$results = array_values(array_filter($results, function ($result) {return (!in_array('read', $result['restrictions']));}));
		
		return array("code" => 200, "data" => sanitize($resource, $results), "last_row" => $last_row, "last_page" => $last_page);
	}
}


function post()
{
	global $optimus_connection, $resource, $input;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'integer', true);
	$input->db = get_user_db($input->owner);
	
	check_input_body($resource, __METHOD__);
	
	$restrictions = get_restrictions($input->user->id, $input->owner, 'calendars');
	if (in_array('create', $restrictions))
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour créer un agenda chez cet utilisateur");

	$query = datatables_insert($optimus_connection, $resource, $input->db, 'calendars');

	if($query->execute())
	{
		$input->body = json_decode('{"filter": [{"field": "id", "type": "=", "value": ' . $optimus_connection->lastInsertId() . '}]}', null, 512, JSON_THROW_ON_ERROR);
		$results = datatable_request($optimus_connection, $resource, $input->db, 'calendars');
		return array("code" => 201, "data" => sanitize($resource, array_merge($results[0], ['restrictions' => $restrictions])));
	}
}


function patch()
{
	global $optimus_connection, $resource, $input;
	auth();
	allowed_origins_only();

	$input->id = check('id', $input->path[3], 'integer', true);
	$input->owner = check('owner', $input->path[1], 'integer', true);
	$input->db = get_user_db($input->owner);
	
	check_input_body($resource, __METHOD__);

	if (count(array_intersect(array_keys((array)$input->body),array_keys(array_filter((array)$resource, function($item){return !in_array('immutable', (array)$item->patch);})))) == 0) 
		return array("code" => 400, "message" => "Aucune donnée à modifier n'a été transmise");

	$restrictions = get_restrictions($input->user->id, $input->owner, 'calendars/' . $input->id);
	if (in_array('write', $restrictions))
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour modifier cet agenda");
	
	$exists = $optimus_connection->query("SELECT id, `server` FROM `" . $input->db . "`.`calendars` WHERE `server` IS NULL AND id = '" . $input->id . "'");
	if ($exists->rowCount() == 0)
		return array("code" => 404, "message" => "Cet agenda n'existe pas");

	$query = datatables_update($optimus_connection, $resource, $input->db, 'calendars', $input->id);
	if($query->execute())
	{
		$input->body = json_decode('{"filter": [{"field": "id", "type": "=", "value": ' . $optimus_connection->lastInsertId() . '}]}', null, 512, JSON_THROW_ON_ERROR);
		$results = datatable_request($optimus_connection, $resource, $input->db, 'calendars');
		return array("code" => 200, "data" => sanitize($resource, array_merge($results[0], ['restrictions' => $restrictions])));
	}
}


function delete()
{
	global $optimus_connection, $input;
	auth();
	allowed_origins_only();
	
	$input->id = check('id', $input->path[3], 'integer', true);
	$input->owner = check('owner', $input->path[1], 'integer', true);
	$input->db = get_user_db($input->owner);

	$restrictions = get_restrictions($input->user->id, $input->owner, 'calendars/' . $input->id);
	if (in_array('delete', $restrictions))
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour supprimer cet agenda");

	if (!exists($optimus_connection, $input->db, 'calendars', 'id', $input->id))
		return array("code" => 404, "message" => "Cet agenda n'existe pas");
	
	$calendar_delete = $optimus_connection->query("DELETE FROM `" . $input->db . "`.`calendars` WHERE id = '" . $input->id . "'");
	$events_delete = $optimus_connection->query("DELETE FROM `" . $input->db . "`.calendars_events WHERE calendar = '" . $input->id . "'");
	
	return array("code" => 200);
}
?>
