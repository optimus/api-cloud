<?php
include_once 'api_optimus-server/datatables.php';
$resource = json_decode('
{
	"id": { "type": "integer", "post": ["ignored"], "patch": ["immutable"], "default": 0},
	"calendar": { "type": "integer", "patch": ["notnull", "notempty"], "default": 1},
	"title": { "type": "string", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": "Evènement N° "},
	"notes": { "type": "text", "default": null},
	"start": { "type": "datetime", "post": ["required"], "patch": ["notnull", "notempty"], "default": "' . date('Y-m-d h:i:s') . '"},
	"end": { "type": "datetime", "post": ["required"], "patch": ["notnull", "notempty"], "default": "' . date('Y-m-d h:i:s') . '"},
	"allday": { "type": "boolean", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": 0},
	"rrule": { "type": "rrule", "default": null},
	"exdate": { "type": "string", "default": null},
	"properties": { "type": "string", "default": null}
}
', null, 512, JSON_THROW_ON_ERROR);

function get()
{
	global $optimus_connection, $resource, $input;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'integer', true);
	$input->db = get_user_db($input->owner);
	$input->calendar = check('calendar', $input->path[3], 'integer', false);
	if ($input->path[5])
		$input->id = check('id', $input->path[5], 'integer', false);
	
	$input->start = check('start', $input->body->start, 'date', false);
	$input->end = check('end', $input->body->end, 'date', false);
	$input->search = check('search', $input->body->search, 'string', false);

	if ($input->user->id != $input->owner AND !is_admin($input->user->id))
	{
		$restrictions = get_restrictions($input->user->id, $input->owner, 'calendars/' . $input->calendar);
		if (in_array('read', $restrictions))
			return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour lire cet agenda");
	}

	if (isset($input->start) AND !isset($input->end))
		$input->end = '9999-12-31 23:59:59';

	if (isset($input->end) AND !isset($input->start))
		$input->start = '1000-01-01 00:00:00';

	if (isset($input->start))
	{
		$start = new DateTimeImmutable($input->start.'+00:00');
		$start = $start->getTimestamp();
	}

	if (isset($input->end))
	{
		$end = new DateTimeImmutable($input->end.'+00:00');
		$end = $end->getTimestamp();
	}
	
	if (isset($input->start) AND isset($input->end) AND $start > $end)
		return array("code" => 400, "message" => "La date 'start' fournie est ultérieure à la date 'end' fournie");

	$calendar = $optimus_connection->query("SELECT * FROM `" . $input->db . "`.`calendars` WHERE id = " . $input->calendar)->fetch(PDO::FETCH_ASSOC);;
	if (!$calendar)
		return array("code" => 404, "message" => "Cet agenda n'existe pas");
	else if ($calendar['server'] != '')
		return array("code" => 404, "message" => "Cet agenda n'est pas stocké sur ce serveur");

	if (isset($input->id))
	{
		$calendars_events = $optimus_connection->prepare("SELECT `id`, `calendar`, `title`, `notes`, DATE_FORMAT(`start`, '%Y-%m-%dT%TZ') AS `start`, DATE_FORMAT(`end`, '%Y-%m-%dT%TZ') AS `end`, `allday`, `rrule` FROM `" . $input->db . "`.`calendars_events` WHERE id = :id");
		$calendars_events->bindParam(':id', $input->id, PDO::PARAM_INT);
	}
	else
	{
		$query = "SELECT `id`, `calendar`, `title`, `notes`, DATE_FORMAT(`start`, '%Y-%m-%dT%TZ') AS `start`, DATE_FORMAT(`end`, '%Y-%m-%dT%TZ') AS `end`, `allday`, `rrule` FROM `" . $input->db . "`.`calendars_events` WHERE calendar = :calendar";
		if (isset($input->start) AND isset($input->end))
			$query .= " AND ((`start` >= :start AND `start` <= :end) OR (`end` >= :start AND `end` <= :end) OR (`start` <= :start AND `end` >= :end) OR `rrule` IS NOT NULL)";
		if (isset($input->search))
			$query .= " AND `title` LIKE '%" . $input->search . "%'";
		$calendars_events = $optimus_connection->prepare($query);
		
		$calendars_events->bindParam(':calendar', $input->calendar, PDO::PARAM_INT);
		if (isset($input->start))
			$calendars_events->bindValue(':start', $input->start . ' 00:00:00', PDO::PARAM_STR);
		if (isset($input->end))
			$calendars_events->bindValue(':end', $input->end . ' 23:59:59', PDO::PARAM_STR);
	}

	if ($calendars_events->execute())
		if ($calendars_events->rowCount() == 0)
			if (isset($input->id))
				return array("code" => 404, "message" => "Cet évènement n'existe pas");
			else
				return array("code" => 204, "message" => "Aucun évènement ne correspond aux critères de recherche");
		else
		{
			$events = $calendars_events->fetchAll(PDO::FETCH_ASSOC);

			//RRULE
			for ($i=0; $i < sizeof($events); $i++)
			{
				if ($events[$i]['allday'] == 1)
				{
					$events[$i]['start'] = substr($events[$i]['start'],0,10);
					$events[$i]['end'] = substr($events[$i]['end'],0,10);
				}

				$event_start = new DateTimeImmutable($events[$i]['start'].'+00:00');
				$event_start = $event_start->getTimestamp();
				$event_end = new DateTimeImmutable($events[$i]['end'] . '+00:00');
				$event_end = $event_end->getTimestamp();
				$event_duration = $event_end - $event_start;
				$events[$i]['duration'] = $event_duration * 1000;

				$events[$i]['exdate'] = null;
				
				if ($events[$i]['rrule'] != '')
				{
					if (!strpos($events[$i]['rrule'], 'RRULE:'))
						$events[$i]['rrule'] = 'RRULE:' . $events[$i]['rrule'];
					
					//EXDATES
					$exdates = $optimus_connection->query("SELECT DATE_FORMAT(`date`, '%Y-%m-%dT%TZ') as `date` FROM `" . $input->db . "`.`calendars_events_exdates` WHERE event = '" . $events[$i]['id'] . "'");
					while ($exdate = $exdates->fetch(PDO::FETCH_ASSOC))
						if ($events[$i]['allday'] == 1)
							$events[$i]['exdate'][] = substr($exdate['date'],0,10);
						else
							$events[$i]['exdate'][] = $exdate['date'];
					if (sizeof($events[$i]['exdate']) > 0)
						$events[$i]['exdate'] = 'EXDATE:' . implode(',',$events[$i]['exdate']);

					if (isset($input->start) AND isset($input->end))
					{
						require_once('api_optimus-server/libs/When/Valid.php');
						require_once('api_optimus-server/libs/When/When.php');

						$r = new When();
						$occurrences = $r->startDate(new DateTime($events[$i]['start']))->rrule($events[$i]['rrule']);
						if ($events[$i]['exdate'])
							$occurrences = $occurrences->exclusions(implode(',',$exdates->fetchAll(PDO::FETCH_ASSOC)));
						$r->rangeLimit = 1100;
						$occurrences = $occurrences->generateOccurrences();
						$occurrences = $r->occurrences;

						//print_r($occurrences);
	
						$has_occurrences_in_search_interval = 0;
						foreach ($occurrences as $occurrence)
						{
							$occurrence_start = $occurrence->getTimestamp();
							$occurrence_end = $occurrence_start + $event_duration;
							if (($occurrence_start >= $start AND $occurrence_start <= $end) OR ($occurrence_end >= $start AND $occurrence_end <= $end) OR ($occurrence_start <= $start AND $occurrence_end >= $end))
							{
								$has_occurrences_in_search_interval++;
								break;
							}
						}
						if ($has_occurrences_in_search_interval == 0)
							$events[$i] = null;
					}
				}
				else
					$events[$i]['exdate'] = null;
			}

			//EXTENDED PROPERTIES
			for ($i=0; $i < sizeof($events); $i++)
			{
				$events[$i]['properties'] = null;
				$properties = $optimus_connection->query("SELECT `property`, `value` FROM `" . $input->db . "`.`calendars_events_properties` WHERE event = '" . $events[$i]['id'] . "'");
				while ($property = $properties->fetch(PDO::FETCH_ASSOC))
					$events[$i]['properties'][$property['property']] = $property['value'];
			}

			$events = array_values(array_filter($events));
			return array("code" => 200, "data" => sanitize($resource, $events), "restrictions" => $restrictions);
		}
	else
		return array("code" => 400, "message" => $calendars_events->errorInfo()[2]);
}


function post()
{
	global $optimus_connection, $resource, $input;
	auth();
	allowed_origins_only();

	

	$input->owner = check('owner', $input->path[1], 'integer', true);
	$input->db = get_user_db($input->owner);
	$input->calendar = check('calendar', $input->path[3], 'integer', true);
	$input->body->calendar = $input->calendar;
	
	if (isset($input->body->start))
		$input->body->start = str_replace('T',' ',str_replace('Z','',$input->body->start));
	if ($input->body->allday == 1)
		$resource->start->type = 'date';

	if (isset($input->body->end))
		$input->body->end = str_replace('T',' ',str_replace('Z','',$input->body->end));
	if ($input->body->allday == 1)
		$resource->end->type = 'date';

	if (isset($input->body->rrule))
	{
		$input->body->rrule = str_replace('RRULE:', '', preg_replace('/\s+/', '', $input->body->rrule));
		if (stripos( $input->body->rrule, 'COUNT=') === false AND stripos( $input->body->rrule, 'UNTIL=') === false)
			output(["code" => 400, "message" =>  $input->body->rrule . " n'est pas une règle rrule acceptée", "detail" => "L'API exige que les évènements récurrents aient une fin (paramètre COUNT ou UNTIL renseignés)"]);
		else if (stripos( $input->body->rrule, 'COUNT=') !== false)
		{
			$count = explode(';', explode('COUNT=',$input->body->rrule)[1])[0];
			if ($count < 1 OR $count > 999)
				output(["code" => 400, "message" => $input->body->rrule . " n'est pas une règle rrule acceptée", "detail" => "L'API exige que COUNT soit un nombre entier compris entre 1 et 999 inclus"]);
		}
		else if (stripos($input->body->rrule, 'UNTIL=') != false)
		{
			$freq = explode(';', explode('FREQ=',$input->body->rrule)[1])[0];
			$until = explode(';', explode('UNTIL=',$input->body->rrule)[1])[0];
			$until = strtotime($until);
			$limit = array("DAILY" => 86313600, "WEEKLY" => 604195200, "MONTHLY" => 2675721600, "YEARLY" => 31504464000);
			$start = strtotime($input->body->start);
			if ($until <= $start)
				output(["code" => 400, "message" =>  $input->body->rrule . " n'est pas une règle rrule valable", "detail" => "L'API exige qu'UNTIL soit une date ultérieure à la première occurence"]);
			else if ($until > ($start + $limit[$freq]))
				output(["code" => 400, "message" =>  $input->body->rrule . " n'est pas une règle rrule valable", "detail" => "L'API exige qu'UNTIL ne génère pas plus de 1000 répétitions"]);
		}
	}

	
	
	//CHECK EXDATES
	if (isset($input->body->exdate))
		$input->body->exdate = str_replace('EXDATE:', '', preg_replace('/\s+/', '', $input->body->exdate));

	check('exdate', $input->body->exdate, 'string', false);
	if (isset($input->body->exdate))
		$exdates = explode(',', $input->body->exdate);
	unset($input->body->exdate);
	foreach ($exdates as $exdate)
		if ($input->body->allday == 1)
			check('exdate', str_replace('T',' ',str_replace('Z','',$exdate)), 'date', false);
		else
			check('exdate', str_replace('T',' ',str_replace('Z','',$exdate)), 'datetime', false);
	
	//CHECK PROPERTIES
	check('properties', $input->body->properties, 'string', false);
	if (isset($input->body->properties))
	$properties = $input->body->properties;
	unset($input->body->properties);
	foreach ($properties as $property => $value)
	{
		check('property', $property, 'module', false);
		check('value', $value, 'string', false);
	}
	
	//CHECK BODY
	check_input_body($resource, __METHOD__);

	//CHECK AUTHORIZATIONS
	if ($input->user->id != $input->owner AND !is_admin($user_id))
	{
		$restrictions = get_restrictions($input->user->id, $input->owner, 'calendars/' . $input->calendar);
		if (in_array('write', $restrictions))
			return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour ajouter des évènements dans cet agenda");
	}

	//CHECK IF CALENDAR EXISTS
	$calendar = $optimus_connection->query("SELECT * FROM `" . $input->db . "`.`calendars` WHERE id = " . $input->calendar)->fetch(PDO::FETCH_ASSOC);;
	if (!$calendar)
		return array("code" => 404, "message" => "Cet agenda n'existe pas");
	else if ($calendar['server'] != '')
		return array("code" => 404, "message" => "Cet agenda n'est pas stocké sur ce serveur");

	//EXECUTE QUERY
	$query = datatables_insert($optimus_connection, $resource, $input->db, 'calendars_events');
	if($query->execute())
	{
		//RECUPERE LES DONNEES DU NOUVEL EVENEMENT CREE
		$new_id = $optimus_connection->lastInsertId();
		if ($input->body->title == $resource->title->default)
			$update_evenement = $optimus_connection->query("UPDATE `" . $input->db . "`.`calendars_events` SET title = CONCAT(title, '"  . $new_id . "') WHERE id = " . $new_id);
		$new_evenement = $optimus_connection->query("SELECT `id`, `calendar`, `title`, `notes`, DATE_FORMAT(`start`, '%Y-%m-%dT%TZ') AS `start`, DATE_FORMAT(`end`, '%Y-%m-%dT%TZ') AS `end`, `allday`, `rrule` FROM `" . $input->db . "`.`calendars_events` WHERE id = " . $new_id)->fetch(PDO::FETCH_ASSOC);
		
		//CALCUL DE LA DUREE DE L'EVENEMENT
		$event_start = new DateTimeImmutable($new_evenement['start']);
		$event_start = $event_start->getTimestamp();
		$event_end = new DateTimeImmutable($new_evenement['end']);
		$event_end = $event_end->getTimestamp();
		$event_duration = $event_end - $event_start;
		$new_evenement['duration'] = $event_duration * 1000;
		
		//FILTRE LES EXDATES ENVOYEES QUI SONT HORS RANGE
		if (sizeof($exdates) > 0)
		{
			require_once('api_optimus-server/libs/When/Valid.php');
			require_once('api_optimus-server/libs/When/When.php');
			$r = new When();
			$occurrences = $r->startDate(new DateTime($new_evenement['start']))->rrule($new_evenement['rrule']);
			$occurrences = $occurrences->generateOccurrences();
			$occurrences = $r->occurrences;
			$last_occurrence = new DateTimeImmutable(end($occurrences)->format('Y-m-d\TH:i:s\Z'));
			$last_occurrence = $last_occurrence->getTimestamp();
			foreach($exdates as $exdate)
			{
				$exdate_timestamp = new DateTimeImmutable($exdate);
				$exdate_timestamp = $exdate_timestamp->getTimestamp();
				if ($exdate_timestamp >= $event_start AND $exdate_timestamp <= $last_occurrence)
					$purged_exdates[] = $exdate;
			}	
		}

		//INSERT EXDATES IN SPECIFIC DATABASE
		foreach($purged_exdates as $exdate)
		{
			$evenement_exdates = $optimus_connection->prepare("INSERT INTO `" . $input->db . "`.`calendars_events_exdates` SET event = :event, date = :date");
			$evenement_exdates->bindParam(':event', $new_id, 1);
			if ($input->body->allday == 1)
				$exdate = substr($exdate,0,10);
			$evenement_exdates->bindParam(':date', str_replace('Z','',$exdate), 2);
			if (!$evenement_exdates->execute())
				return array("code" => 400, "message" => $evenement_exdates->errorInfo()[2]);
		}

		//INSERT PROPERTIES IN SPECIFIC DATABASE
		foreach($properties as $property => $value)
		{
			$evenement_property = $optimus_connection->prepare("INSERT INTO `" . $input->db . "`.`calendars_events_properties` SET event = :event, property = :property, value = :value");
			$evenement_property->bindParam(':event', $new_id, 1);
			$evenement_property->bindParam(':property', $property, 2);
			$evenement_property->bindParam(':value', $value, 2);
			if (!$evenement_property->execute())
				return array("code" => 400, "message" => $evenement_property->errorInfo()[2]);
		}
		
		//SANITIZE OUTPUT
		$new_evenement['rrule'] = $new_evenement['rrule'] ? "RRULE:" . $new_evenement['rrule'] : null;
		$new_evenement['exdate'] = $purged_exdates ? "EXDATE:" . implode(',', $purged_exdates) : null;
		$new_evenement['properties'] = $properties;
		return array("code" => 201, "data" => sanitize($resource, $new_evenement));
	}
	else
		return array("code" => 400, "message" => $query->errorInfo()[2]);
}


function patch()
{
	global $optimus_connection, $resource, $input;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'integer', true);
	$input->calendar = check('calendar', $input->path[3], 'integer', true);
	$input->db = get_user_db($input->owner);
	$input->id = check('id', $input->path[5], 'integer', true);

	if ($input->user->id != $input->owner AND !is_admin($user_id))
	{
		$restrictions = get_restrictions($input->user->id, $input->owner, 'calendars/' . $input->calendar);
		if (in_array('write', $restrictions))
			return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour modifier des évènements dans cet agenda");
	}

	$calendar = $optimus_connection->query("SELECT * FROM `" . $input->db . "`.`calendars` WHERE id = " . $input->calendar)->fetch(PDO::FETCH_ASSOC);
	if (!$calendar)
		return array("code" => 404, "message" => "Cet agenda n'existe pas");
	else if ($calendar['server'] != '')
		return array("code" => 404, "message" => "Cet agenda n'est pas stocké sur ce serveur");
	
	if (!exists($optimus_connection, $input->db, 'calendars_events', 'id', $input->id))
		return array("code" => 404, "message" => "Cet évènement n'existe pas");
	else
		$evenement = $optimus_connection->query("SELECT * FROM `" . $input->db . "`.`calendars_events` WHERE id = '" . $input->id . "'")->fetch(PDO::FETCH_ASSOC);		

	if (isset($input->body->start))
		$input->body->start = str_replace('T',' ',str_replace('Z','',$input->body->start));
	if ($input->body->allday == 1 OR (!isset($input->body->allday) AND $evenement['allday'] == 1))
		$resource->start->type = 'date';

	if (isset($input->body->end))
		$input->body->end = str_replace('T',' ',str_replace('Z','',$input->body->end));
		if ($input->body->allday == 1 OR (!isset($input->body->allday) AND $evenement['allday'] == 1))
		$resource->end->type = 'date';

	if (isset($input->body->rrule))
	{
		$input->body->rrule = str_replace('RRULE:', '', preg_replace('/\s+/', '', $input->body->rrule));
		if (stripos( $input->body->rrule, 'COUNT=') === false AND stripos( $input->body->rrule, 'UNTIL=') === false)
			output(["code" => 400, "message" =>  $input->body->rrule . " n'est pas une règle rrule acceptée", "detail" => "L'API exige que les évènements récurrents aient une fin (paramètre COUNT ou UNTIL renseignés)"]);
		else if (stripos( $input->body->rrule, 'COUNT=') !== false)
		{
			$count = explode(';', explode('COUNT=',$input->body->rrule)[1])[0];
			if ($count < 1 OR $count > 999)
				output(["code" => 400, "message" => $input->body->rrule . " n'est pas une règle rrule acceptée", "detail" => "L'API exige que COUNT soit un nombre entier compris entre 1 et 999 inclus"]);
		}
		else if (stripos( $input->body->rrule, 'UNTIL=') != false)
		{
			$freq = explode(';', explode('FREQ=',$input->body->rrule)[1])[0];
			$until = explode(';', explode('UNTIL=',$input->body->rrule)[1])[0];
			$until = strtotime($until);
			$limit = array("DAILY" => 86313600, "WEEKLY" => 604195200, "MONTHLY" => 2675721600, "YEARLY" => 31504464000);
			$start = strtotime($input->body->start ? $input->body->start : $evenement['start']);
			if ($until <= $start)
				output(["code" => 400, "message" =>  $input->body->rrule . " n'est pas une règle rrule valable", "detail" => "L'API exige qu'UNTIL soit une date ultérieure à la première occurence"]);
			else if ($until > ($start + $limit[$freq]))
				output(["code" => 400, "message" =>  $input->body->rrule . " n'est pas une règle rrule valable", "detail" => "L'API exige qu'UNTIL ne génère pas plus de 1000 répétitions"]);
		}
	}

	//CHECK BODY
	check_input_body($resource, __METHOD__);

	//CHECK EXDATES
	if (isset($input->body->exdate))
		$input->body->exdate = str_replace('EXDATE:', '', preg_replace('/\s+/', '', $input->body->exdate));

	if (isset($input->body->exdate))
		$exdates = explode(',', $input->body->exdate);
	unset($input->body->exdate);
	foreach ($exdates as $exdate)
		if ($input->body->allday == 1 OR (!isset($input->body->allday) AND $evenement['allday'] == 1))
			check('exdate', str_replace('T',' ',str_replace('Z','',$exdate)), 'date', false);
		else
			check('exdate', str_replace('T',' ',str_replace('Z','',$exdate)), 'datetime', false);

	//CHECK PROPERTIES
	if (isset($input->body->properties))
		$properties = $input->body->properties;
	unset($input->body->properties);
	foreach ($properties as $property => $value)
	{
		check('property', $property, 'module', false);
		check('value', $value, 'string', false);
	}
	
	//UPDATE (OR BYPASS IF NOTHING TO UPDATE)
	if (count(array_intersect(array_keys((array)$input->body),array_keys(array_filter((array)$resource, function($item){return !in_array('immutable', (array)$item->patch);})))) == 0)
		$query = 'nothing_to_update';
	else
		$query = datatables_update($optimus_connection, $resource, $input->db, 'calendars_events', $input->id);

	if($query == 'nothing_to_update' OR $query->execute())
	{
		//RECUPERE LES DONNEES DE L'EVENEMENT
		$new_evenement = $optimus_connection->query("SELECT `id`, `calendar`, `title`, `notes`, DATE_FORMAT(`start`, '%Y-%m-%dT%TZ') AS `start`, DATE_FORMAT(`end`, '%Y-%m-%dT%TZ') AS `end`, `allday`, `rrule` FROM `" . $input->db . "`.`calendars_events` WHERE id = " . $input->id)->fetch(PDO::FETCH_ASSOC);
		
		//CALCUL DE LA DUREE DE L'EVENEMENT
		$event_start = new DateTimeImmutable($new_evenement['start']);
		$event_start = $event_start->getTimestamp();
		$event_end = new DateTimeImmutable($new_evenement['end']);
		$event_end = $event_end->getTimestamp();
		$event_duration = $event_end - $event_start;
		$new_evenement['duration'] = $event_duration * 1000;

		//SI UN NOUVEL EXDATE EST ENVOYE
		if ($exdates && $new_evenement['rrule'] != '')
		{
			//FILTRE LES EXDATES ENVOYEES QUI SONT HORS RANGE
			require_once('api_optimus-server/libs/When/Valid.php');
			require_once('api_optimus-server/libs/When/When.php');
			$r = new When();
			$occurrences = $r->startDate(new DateTime($new_evenement['start']))->rrule($new_evenement['rrule']);
			$occurrences = $occurrences->generateOccurrences();
			$occurrences = $r->occurrences;
			$last_occurrence = new DateTimeImmutable(end($occurrences)->format('Y-m-d\TH:i:s\Z'));
			$last_occurrence = $last_occurrence->getTimestamp();
			
			foreach($exdates as $exdate)
			{
				$exdate_timestamp = new DateTimeImmutable($exdate);
				$exdate_timestamp = $exdate_timestamp->getTimestamp();
				if ($exdate_timestamp >= $event_start AND $exdate_timestamp <= $last_occurrence)
					$purged_exdates[] = $exdate;
			}
			//AJOUTE LES NOUVELLES EXDATES RECUES
			foreach($purged_exdates as $exdate)
			{
				$evenement_exdates = $optimus_connection->prepare("INSERT INTO `" . $input->db . "`.`calendars_events_exdates` SET event = :event, date = :date");
				$evenement_exdates->bindParam(':event', $input->id, 1);
				$evenement_exdates->bindParam(':date', str_replace('Z','',$exdate), 2);
				if ($evenement_exdates->execute())
					$new_evenement['exdate'] = $purged_exdates;
				else
					return array("code" => 400, "message" => $evenement_exdates->errorInfo()[2]);
			}
		}

		//SI START OU ALLDAY EST MODIFIE, ALORS QU'IL EXISTE UN RRULE
		if ((isset($input->body->start) || isset($input->body->allday)) && $new_evenement['rrule'] != '')
		{
			//MISE A JOUR DES EXDATES EXISTANTES POUR TENIR COMPTE DE LA NOUVELLE HEURE DE DEPART
			require_once('api_optimus-server/libs/When/Valid.php');
			require_once('api_optimus-server/libs/When/When.php');
			$r = new When();
			$occurrences = $r->startDate(new DateTime($new_evenement['start']))->rrule($new_evenement['rrule']);
			$occurrences = $occurrences->generateOccurrences();
			$occurrences = $r->occurrences;
			$last_occurrence = end($occurrences)->format('Y-m-d H:i:s');
			$start_hour = $input->body->allday == 1 ? '00:00:00' : $occurrences[0]->format('H:i:s');
			$evenement_exdates_update = $optimus_connection->query("UPDATE `" . $input->db . "`.`calendars_events_exdates` SET `date` = CONCAT(date(`date`), ' " . $start_hour . "') WHERE event = '" . $input->id . "'");
			//SUPPRESSION DES EXDATES QUI SONT DEVENUES HORS RANGE
			$evenement_exdates_purge = $optimus_connection->query("DELETE FROM `" . $input->db . "`.`calendars_events_exdates` WHERE event = '" . $input->id . "' AND (`date` < '" . $input->body->start . "' OR `date` > '" . $last_occurrence . "')");
		}
		
		//RETOURNE LA NOUVELLE LISTE DES EXDATES
		$new_exdates = $optimus_connection->query("SELECT `date` FROM `" . $input->db . "`.`calendars_events_exdates` WHERE event = " . $new_evenement['id'])->fetchAll(PDO::FETCH_ASSOC);
			for($i=0; $i < sizeof($new_exdates); $i++)
				if ($new_evenement['allday'] == 1)
					$new_exdates[$i] = substr($new_exdates[$i]['date'], 0, 10);
				else
					$new_exdates[$i] = str_replace(' ','T',$new_exdates[$i]['date']) . 'Z';

		//MISE A JOUR DES PROPERTIES
		$new_evenement['properties'] = null;
		$existing_properties = $optimus_connection->query("SELECT `property`, `value` FROM `" . $input->db . "`.`calendars_events_properties` WHERE event = '" . $input->id . "'");
		while ($existing_property = $existing_properties->fetch(PDO::FETCH_ASSOC))
			$new_evenement['properties'][$existing_property['property']] = $existing_property['value'];
		foreach($properties as $property => $value)
		{
			if ($new_evenement['properties'][$property])
				if (isset($value))
				{
					//SI LA PROPRIETE EXISTE ET QU'UNE VALEUR EST RENSEIGNEE ==> UPDATE
					$evenement_property = $optimus_connection->prepare("UPDATE `" . $input->db . "`.`calendars_events_properties` SET value = :value WHERE event = :event AND property = :property");
					$evenement_property->bindParam(':value', $value, 2);
					$evenement_property->bindParam(':event', $input->id, 1);
					$evenement_property->bindParam(':property', $property, 2);
				}
				else
				{
					//SI LA PROPRIETE EXISTE MAIS QU'AUCUNE VALEUR N'EST RENSEIGNEE (OU SI ELLE EST NULL) ==> DELETE
					$evenement_property = $optimus_connection->prepare("DELETE FROM `" . $input->db . "`.`calendars_events_properties` WHERE event = :event AND property = :property");
					$evenement_property->bindParam(':event', $input->id, 1);
					$evenement_property->bindParam(':property', $property, 2);
				}
			else
			{
				//SI LA PROPRIETE N'EXISTE PAS ENCORE ==> INSERT
				$evenement_property = $optimus_connection->prepare("INSERT INTO `" . $input->db . "`.`calendars_events_properties` SET event = :event, property = :property, value = :value");
				$evenement_property->bindParam(':event', $input->id, 1);
				$evenement_property->bindParam(':value', $value, 2);
				$evenement_property->bindParam(':property', $property, 2);
			}
			if (!$evenement_property->execute())
				return array("code" => 400, "message" => $evenement_property->errorInfo()[2]);
		}

		//RETOURNE LA NOUVELLE LISTE DES PROPERTIES
		$new_evenement['properties'] = null;
		$properties = $optimus_connection->query("SELECT `property`, `value` FROM `" . $input->db . "`.`calendars_events_properties` WHERE event = '" . $input->id . "'");
		while ($property = $properties->fetch(PDO::FETCH_ASSOC))
			$new_evenement['properties'][$property['property']] = $property['value'];

		//FORMATTAGE DES CHAMPS DE RETOUR
		$new_evenement['start'] = $new_evenement['allday'] == 1 ? substr($new_evenement['start'],0,10) : $new_evenement['start'];
		$new_evenement['end'] = $new_evenement['allday'] == 1 ? substr($new_evenement['end'],0,10) : $new_evenement['end'];
		$new_evenement['rrule'] = $new_evenement['rrule'] ? "RRULE:" . $new_evenement['rrule'] : null;
		$new_evenement['exdate'] = $new_exdates ? "EXDATE:" . implode(',', $new_exdates) : null;
		
		return array("code" => 200, "data" => sanitize($resource, $new_evenement));
	}
	else
		return array("code" => 400, "message" => $query->errorInfo()[2]);
}


function delete()
{
	global $optimus_connection, $resource, $input;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'integer', true);
	$input->calendar = check('calendar', $input->path[3], 'integer', true);
	$input->db = get_user_db($input->owner);
	$input->id = check('id', $input->path[5], 'integer', true);

	if ($input->user->id != $input->owner AND !is_admin($user_id))
	{
		$restrictions = get_restrictions($input->user->id, $input->owner, 'calendars/' . $input->calendar);
		if (in_array('write', $restrictions))
			return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour supprimer des évènements dans cet agenda");
	}
	
	$calendar = $optimus_connection->query("SELECT * FROM `" . $input->db . "`.`calendars` WHERE id = " . $input->calendar)->fetch(PDO::FETCH_ASSOC);
	if (!$calendar)
		return array("code" => 404, "message" => "Cet agenda n'existe pas");
	else if ($calendar['server'] != '')
		return array("code" => 404, "message" => "Cet agenda n'est pas stocké sur ce serveur");

	$exists = $optimus_connection->query("SELECT id FROM `" . $input->db . "`.`calendars_events` WHERE id = '" . $input->id . "'");
	if ($exists->rowCount() == 0)
		return array("code" => 404, "message" => "Cet évènement n'existe pas");
	else
		$evenement = $exists->fetchObject();

	$evenement_delete = $optimus_connection->query("DELETE FROM `" . $input->db . "`.`calendars_events` WHERE id = '" . $input->id . "'");
	return array("code" => 200);
}
?>
