<?php
function get()
{
	global $optimus_connection, $input;
	auth();
	allowed_origins_only();

	$path = explode('/',$input->body->path);
	$input->owner = new stdClass;
	$input->owner->id = get_user_id($path[2]);
	$input->owner->db = $path[2];
	$zip_to_create = time() . ".zip";

	validate('owner_id', $input->owner->id, 'integer', true);
	validate('owner_db', $input->owner->db, 'email', true);

	$authorizations = get_rights($input->user->id, $input->owner->id, $input->body->file);
	if ($authorizations['read'] == false OR $authorizations['write'] == false OR $authorizations['create'] == false)
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour effectuer cette action");
	
	if (is_file($input->body->path . '/' . $zip_to_create))
		return array("code" => 409, "message" => "Le fichier de destination existe déjà");

	if (!isset($input->body->path)) 
		return array("code" => 400, "message" => "Aucun chemin n'a été spécifié");
	
	if (strpos('/srv' . $input->body->path,'/..')) 
		return array("code" => 400, "message" => "Le chemin fourni est invalide");
	
	if (!is_dir('/srv' . $input->body->path)) 
		return array("code" => 400, "message" => "Le chemin spécifié n'existe pas");
	
	if (!isset($input->body->items)) 
		return array("code" => 400, "message" => "Aucun fichier ou dossier à compresser n'a été spécifié");

	foreach ($input->body->items as $item)
		if (strpos($input->body->path,'/')) 
			return array("code" => 400, "message" => "Un des fichiers spécifiés contient un chemin invalide");
	
	foreach ($input->body->items as $item)
		if (!file_exists('/srv' . $input->body->path . '/' . $item))
			return array("code" => 400, "message" => "Un des fichiers spécifiés n'existe pas");
	
	exec("LC_ALL=fr_FR.UTF-8 export HOME=/srv/api/tmp && cd '/srv" . $input->body->path . "'; zip -r '" . $zip_to_create . "' '" . implode("' '",$input->body->items) . "' 2>&1",$output);
	
	if (is_file('/srv' . $input->body->path .  '/' . $zip_to_create))
		return array("code" => 200, "message" => "Conversion effectuée avec succès", "data" => $zip_to_create);
	else
		return array("code" => 400, "message" => "La conversion a échoué");
}
?>
