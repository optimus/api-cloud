<?php
function get()
{
	auth();
	allowed_origins_only();
	
	$latest = file_get_contents('https://git.cybertron.fr/optimus/api-cloud/-/raw/master/VERSION');
	$latest = explode("\n",$latest);
	$current = file_get_contents('/srv/api/api_optimus-cloud/VERSION');
	$current = explode("\n",$current);
	$version = array ("name"=>"optimus-cloud", "current_date"=>intval($current[0]), "current_version"=>$current[1], "latest_date"=>intval($latest[0]), "latest_version"=>$latest[1]);
	return array("code" => 200, "data" => $version);
}


function post()
{
	global $connection, $input, $username, $password;
	auth();
	allowed_origins_only();

	if ($input->path[1] == 'service')
	{
		admin_only();

		$actual_version_date = intval(trim(file('/srv/api/api_optimus-cloud/VERSION')[0]));
		$available_version_date = intval(trim(file_get_contents('https://git.cybertron.fr/optimus/api-cloud/-/raw/master/VERSION')));

		if ($available_version_date > $actual_version_date)
		{
			exec('rm -R /srv/api/api_optimus-cloud');
			exec('mkdir /srv/api/api_optimus-cloud');
			exec('git clone https://git.cybertron.fr/optimus/api-cloud /srv/api/api_optimus-cloud/.');

			$new_version_date = intval(trim(file('/srv/api/api_optimus-cloud/VERSION')[0]));

			$user_sql_files = array_diff(scandir('/srv/api/api_optimus-cloud/sql_service'), array('..', '.'));
			$users_query = $connection->query("SELECT TABLE_SCHEMA as db FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'calendars'");
			while($user = $users_query->fetch(PDO::FETCH_ASSOC))
				foreach($user_sql_files as $user_sql_file)
					if (preg_replace("/[^0-9]/","",$user_sql_file) > $actual_version_date)
					{
						$sql = file_get_contents("/srv/api/api_optimus-cloud/sql_user/" . $user_sql_file);
						$sql = explode(';',$sql);
						$connection->exec("USE " . $user['db']);
						foreach ($sql as $instruction)
							if($instruction!='')
								if (!$connection->query($instruction))
									$errors[] = $connection->errorInfo()[2] . "\n";
					}

			if ($errors)
				return array("code" => 400, "message" => implode("\n",$errors));
			else
				return array("code" => 201, "data" => array("old_version"=>$actual_version_date, "new_version"=>$new_version_date), "message" => "Mise à jour effectuée avec succès");
		}
		else
			return array("code" => 200, "data" => array("actual_version"=>$actual_version_date, "available_version"=>$available_version_date), "message" => "Aucune mise à jour n'est disponible");
	}
	else
	{
		validate('owner', $input->path[1], 'integer', true);
		$input->owner = $input->path[1];
		$input->db = get_user_db($input->path[1]);

		if ($input->owner != $input->user->id AND !is_admin($input->user->id))
			return array("code" => 401, "message" => "Seul l'utilisateur lui même ou un administrateur peut installer un service");
		
		$connection->query("CREATE DATABASE IF NOT EXISTS `" . $input->db . "` CHARACTER SET utf8 COLLATE utf8_general_ci");
		$connection->query("USE `" . $input->db . "`");
		$connection->query("REPLACE INTO `server`.`users_services` SET user = '" . $input->owner . "', service = 'optimus-cloud'");
		$connection->query("GRANT SELECT, INSERT, UPDATE, DELETE ON `" . $input->db . "`.* TO '" . $username . "'@'localhost' IDENTIFIED BY '" . $password . "'");
		$connection->query("FLUSH PRIVILEGES");
		$sql_files = array_diff(scandir('/srv/api/api_optimus-cloud/sql_user'), array('..', '.'));
		foreach($sql_files as $sql_file)
		{
			$sql = file_get_contents("/srv/api/api_optimus-cloud/sql_user/" . $sql_file);
			$sql = explode(';',$sql);
			foreach ($sql as $instruction)
				if($instruction!='')
					if (!$connection->query($instruction))
						$errors[] = $connection->errorInfo()[2] . "\n";
		}

		if ($errors)
			return array("code" => 400, "message" => $errors, "data" => array("version_date" => $version[0], "version" => $version[1]));
		else
			
		return array("code" => 201, "data" => array("version_date" => trim(file('/srv/api/api_optimus-cloud/VERSION')[0]), "version" => trim(file('/srv/api/api_optimus-cloud/VERSION')[1])));
	}
}

function delete()
{
	global $connection, $input, $username, $password;
	auth();
	allowed_origins_only();

	validate('owner', $input->path[1], 'integer', true);
	$input->owner = $input->path[1];
	$input->db = get_user_db($input->path[1]);

	if ($input->owner != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Seul l'utilisateur lui même ou un administrateur peut désinstaller un service");

	$sql = <<<EOD
	DROP TABLE `$input->db`.contacts;
	DROP TABLE `$input->db`.calendars;
	DROP TABLE `$input->db`.calendars_events;
	DROP TABLE `$input->db`.calendars_events_exdates;
	DROP TABLE `$input->db`.calendars_events_alarms;
	DROP TABLE `$input->db`.calendars_events_attendees;
	DROP TABLE `$input->db`.calendars_events_properties;
	EOD;

	$sql = explode(';',$sql);
	foreach ($sql as $instruction)
		if($instruction!='')
			if (!$connection->query($instruction))
				$errors[] = $connection->errorInfo()[2] . "\n";
		
	$connection->query("DELETE FROM `server`.`users_services` WHERE user = '" . $input->owner . "' AND service = 'optimus-cloud'");
	$connection->query("REVOKE SELECT, INSERT, UPDATE, DELETE ON `" . $input->db . "`.* TO '" . $username . "'@'localhost' IDENTIFIED BY '" . $password . "'");
	$connection->query("FLUSH PRIVILEGES");
	
	if ($errors)
		return array("code" => 400, "message" => $errors);
	else
		return array("code" => 200);
}
?>