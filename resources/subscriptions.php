<?php
include_once 'api_optimus-server/datatables.php';
$resource = json_decode('
{
	"id": { "type": "integer", "field": "calendars.id", "post": ["ignored"], "patch": ["immutable"], "default": 0},
	"server": { "type": "domain", "field": "calendars.server", "post": ["required"]},
	"user": { "type": "integer", "field": "calendars.user", "post": ["required"]},
	"calendar": { "type": "integer", "field": "calendars.calendar", "post": ["required"]},
	"name": { "type": "string", "field": "calendars.name", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": "NOUVEL AGENDA ' . time() . '"},
	"color": { "type": "hexcolor", "field": "calendars.color", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": "485fc7"},
	"display": { "type": "boolean", "field": "calendars.display", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": 1},
	"displayname" : { "type" : "string", "field": "calendars.displayname", "virtual": true }
}
', null, 512, JSON_THROW_ON_ERROR);

function get()
{
	global $optimus_connection, $resource, $input;
	auth();
	allowed_origins_only();

	$input->id = check('id', $input->path[3], 'integer', false);
	$input->owner = check('owner', $input->path[1], 'integer', true);
	$input->db = get_user_db($input->owner);
	
	if ($input->user->id != $input->owner AND !is_admin($input->user->id))
		if (isset($input->id))
		{
			$restrictions = get_restrictions($input->user->id, $input->owner, 'calendars/' . $input->id);
			if (in_array('read', $restrictions))
				return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour lire cette souscription");
		}
		else
		{
			$restrictions = get_restrictions_list($input->user->id, $input->owner, 'calendars');
			if (sizeof($restrictions) > 0 AND array_count_values(array_column($restrictions, 0))['read'] == sizeof($restrictions))
				return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour lister les souscriptions de cet utilisateur");
		}
	
	//REQUETE SUR UNE SOUSCRIPTION IDENTIFIÉE
	if (isset($input->id))
	{
		$input->body = json_decode('{"filter": [{"field": "id", "type": "=", "value": ' . $input->id . '}, {"field": "server", "type": "isnotnull"}]}', null, 512, JSON_THROW_ON_ERROR);
		$results = datatable_request($optimus_connection, $resource, $input->db, 'calendars');
		$results[0]['owner']['id'] = (int)$results[$i]['user'];
		$results[0]['owner']['server'] = $results[$i]['server'];
		$results[0]['owner']['calendar'] = (int)$results[$i]['calendar'];
		unset($results[0]['server']);
		unset($results[0]['user']);
		unset($results[0]['calendar']);
		if (sizeof($results) == 0)
			return array("code" => 404, "message" => "Cet agenda n'existe pas");
		else
			return array("code" => 200, "data" => sanitize($resource, array_merge($results[0], ['restrictions' => $restrictions])));
	}
	//REQUETE SUR TOUTES LES SOUSCRIPTIONS AU FORMAT DATATABLES
	else 
	{	
		$input->body->filter[] = (object)array("field"=> "server", "type"=> "isnotnull");
		$results = datatable_request($optimus_connection, $resource, $input->db, 'calendars', $restrictions);
		$last_row = (int)$optimus_connection->query('SELECT FOUND_ROWS()')->fetchColumn();
		$last_page = $input->body->size > 0 ? ceil(max($last_row,1) / $input->body->size) : 1;
		for ($i=0; $i < sizeof($results); $i++)
		{
			$results[$i]['owner']['id'] = (int)$results[$i]['user'];
			$results[$i]['owner']['server'] = $results[$i]['server'];
			$results[$i]['owner']['calendar'] = (int)$results[$i]['calendar'];
			unset($results[$i]['server']);
			unset($results[$i]['user']);
			unset($results[$i]['calendar']);
			$results[$i]['restrictions'] = $restrictions['calendars/' . $results[$i]['id']] ?? array_diff($restrictions['calendars'],['create']);
		}
		$results = array_values(array_filter($results, function ($result) {return (!in_array('read', $result['restrictions']));}));
		return array("code" => 200, "data" => sanitize($resource, $results), "last_row" => $last_row, "last_page" => $last_page);
	}
}


function post()
{
	global $optimus_connection, $resource, $input;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'integer', true);
	$input->db = get_user_db($input->owner);
	
	check_input_body($resource, __METHOD__);

	if (!is_admin($input->user->id) && $input->user->id != $input->owner)
		return array("code" => 403, "message" => "Seul un administrateur ou le propriétaire lui même peuvent créer une souscription");
	
	//$restrictions = get_restrictions($input->user->id, $input->owner, 'calendars');
	//if (in_array('create', $restrictions))
		//return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour créer une souscription");
	
	$already_exists = $optimus_connection->prepare("SELECT id FROM `" . $input->db . "`.`calendars` WHERE server=:server AND user=:user AND calendar=:calendar");
	$already_exists->bindParam(':server', $input->body->server, PDO::PARAM_STR);
	$already_exists->bindParam(':user', $input->body->user, PDO::PARAM_INT);
	$already_exists->bindParam(':calendar', $input->body->calendar, PDO::PARAM_INT);
	$already_exists->execute();
	if ($already_exists->rowCount() > 0)
		return array("code" => 400, "message" => "Cette souscription existe déjà");

	if ($input->body->server == $_SERVER['HTTP_HOST'])
	{
		$target_exists = $optimus_connection->prepare("SELECT id FROM `" . get_user_db($input->body->user) . "`.`calendars` WHERE id=:id AND server IS NULL");
		$target_exists->bindParam(':id', $input->body->calendar, PDO::PARAM_INT);
		$target_exists->execute();
		if ($target_exists->rowCount() == 0)
			return array("code" => 400, "message" => "L'agenda externe désigné n'existe pas sur ce serveur");
	}

	$query = datatables_insert($optimus_connection, $resource, $input->db, 'calendars');
	if($query->execute())
	{
		$input->body = json_decode('{"filter": [{"field": "id", "type": "=", "value": ' . $optimus_connection->lastInsertId() . '}]}', null, 512, JSON_THROW_ON_ERROR);
		$results = datatable_request($optimus_connection, $resource, $input->db, 'calendars');
		$results[0]['owner']['id'] = (int)$results[0]['user'];
		$results[0]['owner']['server'] = $results[0]['server'];
		$results[0]['owner']['calendar'] = (int)$results[0]['calendar'];
		unset($results[0]['server']);
		unset($results[0]['user']);
		unset($results[0]['calendar']);
		return array("code" => 201, "data" => sanitize($resource, array_merge($results[0], ['restrictions' => $restrictions])));
	}
}


function patch()
{
	global $optimus_connection, $resource, $input;
	auth();
	allowed_origins_only();

	$input->id = check('id', $input->path[3], 'integer', true);
	$input->owner = check('owner', $input->path[1], 'integer', true);
	$input->db = get_user_db($input->owner);
	
	check_input_body($resource, __METHOD__);

	if (!is_admin($input->user->id) && $input->user->id != $input->owner)
		return array("code" => 403, "message" => "Seul un administrateur ou le propriétaire lui même peuvent modifier une souscription");

	if (count(array_intersect(array_keys((array)$input->body),array_keys(array_filter((array)$resource, function($item){return !in_array('immutable', (array)$item->patch);})))) == 0) 
		return array("code" => 400, "message" => "Aucune donnée à modifier n'a été transmise");

	//$restrictions = get_restrictions($input->user->id, $input->owner, 'calendars/' . $input->id);
	//if (in_array('write', $restrictions))
		//return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour modifier cet agenda");
	
	$exists = $optimus_connection->query("SELECT * FROM `" . $input->db . "`.`calendars` WHERE  `server` IS NOT NULL AND id = '" . $input->id . "'");
	if ($exists->rowCount() == 0)
		return array("code" => 404, "message" => "Cette souscription n'existe pas");
	
	$current_entry = $exists->fetch(PDO::FETCH_ASSOC);
	$server = isset($input->body->server) ? $input->body->server : $current_entry['server'];
	$user = isset($input->body->user) ? $input->body->user : $current_entry['user'];
	$calendar = isset($input->body->calendar) ? $input->body->calendar : $current_entry['calendar'];
	$already_exists = $optimus_connection->prepare("SELECT id FROM `" . $input->db . "`.`calendars` WHERE server=:server AND user=:user AND calendar=:calendar AND id!=:id");
	$already_exists->bindParam(':server', $server, PDO::PARAM_STR);
	$already_exists->bindParam(':user', $user, PDO::PARAM_INT);
	$already_exists->bindParam(':calendar', $calendar, PDO::PARAM_INT);
	$already_exists->bindParam(':id', $input->id, PDO::PARAM_INT);
	$already_exists->execute();
	if ($already_exists->rowCount() > 0)
		return array("code" => 400, "message" => "cette souscription existe déjà");
	
	if (($input->body->server ?: $current_entry['server']) == $_SERVER['HTTP_HOST'])
	{
		$target_exists = $optimus_connection->prepare("SELECT id FROM `" . get_user_db($input->body->user ?: $current_entry['user']) . "`.`calendars` WHERE id=:id AND server IS NULL");
		$target_exists->bindParam(':id', $input->body->calendar, PDO::PARAM_INT);
		$target_exists->execute();
		if ($target_exists->rowCount() == 0)
			return array("code" => 400, "message" => "L'agenda externe désigné n'existe pas sur ce serveur");
	}
	
	$query = datatables_update($optimus_connection, $resource, $input->db, 'calendars', $input->id);
	if($query->execute())
	{
		$input->body = json_decode('{"references": true, "filter": [{"field": "id", "type": "=", "value": ' . $optimus_connection->lastInsertId() . '}]}', null, 512, JSON_THROW_ON_ERROR);
		$results = datatable_request($optimus_connection, $resource, $input->db, 'calendars');
		$results[0]['owner']['id'] = (int)$results[0]['user'];
		$results[0]['owner']['server'] = $results[0]['server'];
		$results[0]['owner']['calendar'] = (int)$results[0]['calendar'];
		unset($results[0]['server']);
		unset($results[0]['user']);
		unset($results[0]['calendar']);
		return array("code" => 200, "data" => sanitize($resource, array_merge($results[0], ['restrictions' => $restrictions])));
	}
}


function delete()
{
	global $optimus_connection, $input;
	auth();
	allowed_origins_only();
	
	$input->id = check('id', $input->path[3], 'integer', true);
	$input->owner = check('owner', $input->path[1], 'integer', true);
	$input->db = get_user_db($input->owner);

	$restrictions = get_restrictions($input->user->id, $input->owner, 'calendars/' . $input->id);
	if (in_array('delete', $restrictions))
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour supprimer cet agenda");

	$exists = $optimus_connection->query("SELECT id, `server` FROM `" . $input->db . "`.`calendars` WHERE  `server` IS NOT NULL AND id = '" . $input->id . "'");
	if ($exists->rowCount() == 0)
		return array("code" => 404, "message" => "Cette souscription n'existe pas");
	else
		$calendar = $exists->fetchObject();
	
	$calendar_delete = $optimus_connection->query("DELETE FROM `" . $input->db . "`.`calendars` WHERE id = '" . $input->id . "'");
	
	return array("code" => 200);
}
?>
